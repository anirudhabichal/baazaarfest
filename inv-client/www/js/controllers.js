angular.module('app.controllers', [])

      .controller('homeCtrl', ['$scope', '$stateParams', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
            // You can include any angular dependencies as parameters for this function
            // TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams, $ionicLoading, $ionicPopup, $state, $rootScope) {
                  $scope.orderReceived = false;
                  $scope.autoOrdering = true;
                  $rootScope.autoOrdering = $scope.autoOrdering;

                  $scope.toggleOrder = function () {
                        $scope.autoOrdering = !$scope.autoOrdering;
                        $rootScope.autoOrdering = $scope.autoOrdering;
                  };

                  $scope.showLoader = function () {
                        $ionicLoading.show({
                              noBackdrop: true, duration: 3000,
                              template: '<ion-spinner icon="lines" class="spinner-energized"></ion-spinner> <br/> Order Received, Checking Inventory'
                        });
                  };

                  $scope.hideLoader = function () {
                        $ionicLoading.hide().then(function () {
                              socket.emit("checkInventory");
                              $state.go("tabsController.inventory");
                        });
                  };

                  socket.on("orderPlaced", function () {
                        $ionicLoading.show();
                        $scope.$apply(function () {
                              $scope.orderReceived = true;
                              $ionicLoading.hide();
                              if ($scope.autoOrdering) {
                                    $scope.showLoader();
                                    setTimeout(function () {
                                          $scope.hideLoader();
                                          // $ionicLoading.hide();
                                    }, 2000);
                              } else {
                                    setTimeout(function () {
                                          var alertPopup = $ionicPopup.alert({
                                                title: 'Order Received. Check Inventory',
                                          });

                                          alertPopup.then(function (res) {
                                                $ionicLoading.show();
                                                setTimeout(function () {
                                                      $ionicLoading.hide();
                                                      socket.emit("checkInventory");
                                                      $state.go("tabsController.inventory");
                                                }, 3000);
                                          });
                                    }, 2000);
                              }
                        });
                  });

            }])

      .controller('inventoryCtrl', ['$scope', '$stateParams', '$ionicLoading', '$ionicPopup', '$state', '$rootScope',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
            // You can include any angular dependencies as parameters for this function
            // TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams, $ionicLoading, $ionicPopup, $state, $rootScope) {
                  console.log("Auto ordering", $rootScope.autoOrdering);
                  $scope.placeAutoOrder = $rootScope.autoOrdering;

                  $scope.autoOrder = function () {
                        if ($rootScope.autoOrdering) {
                              var alertPopup = $ionicPopup.alert({
                                    title: 'Auto Placing Order',
                              });

                              alertPopup.then(function (res) {
                                    $ionicLoading.show();
                                    setTimeout(function () {
                                          $ionicLoading.hide();
                                          $state.go("orderConfirmation");
                                    }, 3000);
                              });
                        } else {
                              $ionicLoading.show();
                              setTimeout(function () {
                                    $ionicLoading.hide();
                                    $state.go("orderConfirmation");
                              }, 3000);
                        }
                  };

                  if ($rootScope.autoOrdering) {
                        $scope.autoOrder();
                  }
            }])

      .controller('ordersCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
            // You can include any angular dependencies as parameters for this function
            // TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams) {


            }])

      .controller('orderConfirmationCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
            // You can include any angular dependencies as parameters for this function
            // TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams) {


            }])

      .controller('loginCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
            // You can include any angular dependencies as parameters for this function
            // TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams) {


            }])
