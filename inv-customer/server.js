const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
var sc;
app.get("/home", (req, resp) => {
      console.log("Here...");
      resp.send("hi");
      var i = 0;
      var isCheckInventory = false;
      io.on('connection', function (socket) {
            sc = socket;
            socket.emit('init', "Hello World");
            socket.on('checkout', function (data) {
                  console.log("Checkout");
                  socket.emit("paymentSuccessful");
                  socket.broadcast.emit("orderPlaced");
            });

            socket.on('invLoaded', function (data) {
                  if (isCheckInventory) {
                        console.log("on inventory");
                        socket.broadcast.emit("inventoryAutoPlaceOrder");
                  }
            });

            socket.on('checkInventory', function (data) {
                  isCheckInventory = true;
            });
      });
});

server.listen(3000);
app.listen(8080, function () {
      console.log("Running on port 8080");
});